Commerce Recipient allows users at checkout to add a separate shipping address
for each line item on their order. This is made for sites that sell gifts that
can be shipped elsewhere, like gift baskets and flowers.

With a regular order, the shipping address can be different from the billing
address, but that is for the whole order. What if each item needed its own
address? Going further, what if each item needed its own gift message? Or some
other bit of unique information?

Recipients are full fieldable entities, so when a customer want to add one to a
line item, it will automatically load any field that you have attached. This
allows for some great flexibility in its use. Out of the box, the recipient
entity has an addressfield attached to it.

It utilizes Views Megarow to load the recipient creation form via AJAX during
the checkout process.

How it Works
-----------------

- Upon installation, an entity reference field is added to every product line
item type that has been defined upon. This will point to an optional recipient
entity.

- A checkbox is added to the Product Type edit form. This will allow you to
select only certain product types that can have individual recipients.

- A new checkout page and pane is added. The pane embeds the Views Megarow table
of the current order's line items. By default, the current Billing Address will
be shown as the recipient for the line item. An "Edit Recipient" link will also
be shown.
