<?php

function commerce_recipient_add($op = 'edit', $line_item_id) {
    if ($op == 'edit') {
        $line_item = commerce_line_item_load($line_item_id);
        $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
        $recipient = $line_item_wrapper->field_recipient_reference->value();
    } elseif ($op == 'add') {
        $recipient = commerce_recipient_new();
    }

    $recipient_form = drupal_get_form('commerce_recipient_form', $recipient, $line_item_id, $op);


    $output = "<div id='commerce-recipient-form'>";
    $output .= drupal_render($recipient_form);
    $output .= "</div>";
    $return = views_megarow_display('Add Recipient', $output, $line_item_id);


    // If a recipient was just saved, close the form and refresh the line item row in the view
    if ($recipient_form['#refresh_line_item_row'] == TRUE) {

        // We need to get the order the line item belongs to
        if (empty($line_item)) {
            $line_item = commerce_line_item_load($line_item_id);
        }
        $return['#commands'][] = views_megarow_command_refresh_parent($line_item_id, 'block', array($line_item->order_id));
        $return['#commands'][] = views_megarow_command_dismiss($line_item_id);
    }


    return $return;
}

/**
 * Generates the entity add/edit form
 *
 */
function commerce_recipient_form($form, &$form_state) {

    $form = array();

    // If we just saved something, we need to let the flag bubble up to the page callback
    if ($form_state['recipient_saved'] == TRUE) {
        $form['#refresh_line_item_row'] = TRUE;
    }

    $recipient = $form_state['build_info']['args'][0];
    $op = $form_state['build_info']['args'][2];

    // attach entity form to get all the attached fields
    field_attach_form('commerce_recipient', $recipient, $form, $form_state);

    // attach submit button
    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
        '#type' => 'submit',
        '#value' => ($op === 'edit') ? 'Save Recipient' : 'Add Recipient',
        '#weight' => 50,
    );

    return $form;
}

/**
 * Form API Submit callback for the Recipient add/edit form
 */
function commerce_recipient_form_submit($form, &$form_state) {
    $recipient = $form_state['build_info']['args'][0];
    $recipient->title = check_plain($form_state['values']['field_recipient_shipping_address'][LANGUAGE_NONE][0]['thoroughfare']);
    field_attach_submit('commerce_recipient', $recipient, $form, $form_state);

    global $user;

    $recipient->uid = $user->uid;

    // save and go back
    commerce_recipient_save($recipient);

    $line_item = commerce_line_item_load($form_state['build_info']['args'][1]);
    $line_item->field_recipient_reference = array(
        LANGUAGE_NONE => array(
            array(
                'target_id' => $recipient->recipient_id,
            )
        )
    );
    commerce_line_item_save($line_item);

    $form_state['rebuild'] = TRUE;
    $form_state['recipient_saved'] = TRUE;
}

function commerce_recipient_load_by_name() {
    return false;
}
