<?php

/**
 * Implements hook_views_data_alter()
 */
function commerce_recipient_views_data_alter(&$data) {
    $data['views_entity_commerce_line_item']['recipient_address'] = array(
        'field' => array(
            'handler' => 'commerce_recipient_handler_recipient_address',
            'title' => t('Recipient Address and Edit Link'),
            'help' => t('The address for the current recipient with the edit link. To be used with Views Megarow tables.'),
        ),
    );
}

