<?php

class commerce_recipient_handler_recipient_address extends views_handler_field_entity {
    function render($values) {
        $element = array();

        // load current line item
        $line_item_wrapper = entity_metadata_wrapper(
            'commerce_line_item',
            commerce_line_item_load($this->get_value($values, 'line_item_id'))
        );

        if ($line_item_wrapper->field_recipient_reference->value() != '') {
            // if user has added a recipient address use that
            $address = $line_item_wrapper->field_recipient_reference->field_recipient_address->value();

            // create an edit recipient link
            $element['edit_recipient'] = array(
                '#markup' => l('Edit Recipient', "commerce_recipient/{$line_item_wrapper->line_item_id->value()}/edit", array(
                    'attributes' => array(
                        'class' => array('views-megarow-open'),
                    ),
                )),
                '#weight' => 100,
                '#prefix' => '<div class="commerce-recipient-operations">',
                '#suffix' => '</div>',
            );
       } else {
            // if user has yet to add a recipient address use the billing address on the order
            $order = entity_metadata_wrapper('commerce_order', $line_item_wrapper->order->value());
            $billing = entity_metadata_wrapper('commerce_customer_profile', $order->commerce_customer_billing->value());
            $address = $billing->commerce_customer_address->value();


            // create an add recipient link
            $element['add_recipient'] = array(
                '#markup' => l('Edit Recipient', "commerce_recipient/{$line_item_wrapper->line_item_id->value()}/add", array(
                    'attributes' => array(
                        'class' => array('views-megarow-open'),
                    )
                )),
                '#weight' => 100,
                '#prefix' => '<div class="commerce-recipient-operations">',
                '#suffix' => '</div>',
            );
        }

        // generate a render array for the address
        $h = array('address' => 'address');
        $element += addressfield_generate($address, $h);

        return $element;
    }
}
