<?php

/**
 * Implements hook_commerce_pane_checkout_form().
 * We embed the Views Megarow table of order line items.
 */

function commerce_recipient_add_recipients_pane_checkout_form($form, $form_state, $checkout_pane, $order) {
    $form['line_item_recipients'] = array(
        '#markup' => commerce_embed_view('line_item_recipients', 'block', array($order->order_id)),
    );

    return $form;
}

